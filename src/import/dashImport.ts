import {
    Float32BufferAttribute,
    MeshBasicMaterial,
    BufferGeometry,
    Mesh,
    SkinnedMesh,
    Texture,
} from 'three'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import debugTexure from './debugTexture'

const debugImage = new Image()
debugImage.src = debugTexure
let dTexture: Texture | null
debugImage.onload = () => {
    const canvas = document.createElement('canvas')
    canvas.width = 256
    canvas.height = 256
    const context = canvas.getContext('2d')
    context?.drawImage(debugImage, 0, 0, 256, 256)
    dTexture = new Texture(canvas)
    dTexture.needsUpdate = true
}

const dashImport = (subject: any): Mesh | SkinnedMesh => {
    // const mats = getDebugMaterials()

    const { vertices, faces, materials } = subject
    // const tex = textures.map((t: any) => {
    //     const image = new Image()
    //     image.src = t.data
    //     const texture = new Texture(image)
    //     //texture.flipY = true
    //     image.onload = () => {
    //         texture.needsUpdate = true
    //     }
    //     return texture
    // })

    const mats = materials.map((m: any) => {
        const material = new MeshBasicMaterial({
            color: 0xffffff,
            map: dTexture,
        })
        return material
    })
    dTexture!.needsUpdate = true

    const pos: number[] = []
    const uvs: number[] = []

    for (let i = 0; i < faces.length; i++) {
        const { a, b, c } = faces[i].indices
        const { diffuseMap } = faces[i]

        pos.push(
            vertices[a].position.x,
            vertices[a].position.y,
            vertices[a].position.z,
            vertices[b].position.x,
            vertices[b].position.y,
            vertices[b].position.z,
            vertices[c].position.x,
            vertices[c].position.y,
            vertices[c].position.z
        )

        uvs.push(
            diffuseMap.a.u,
            diffuseMap.a.v,
            diffuseMap.b.u,
            diffuseMap.b.v,
            diffuseMap.c.u,
            diffuseMap.c.v
        )
    }

    const geometry = new BufferGeometry()
    geometry.setAttribute('position', new Float32BufferAttribute(pos, 3))
    geometry.setAttribute('uv', new Float32BufferAttribute(uvs, 2))
    geometry.computeVertexNormals()
    geometry.addGroup(0, pos.length, 0)
    const mesh = new Mesh(geometry, mats)
    return mesh
}

export default dashImport
