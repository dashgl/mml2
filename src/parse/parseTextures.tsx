export type TimHeader = {
    decompressedSize: number
    paletteX: number
    paletteY: number
    colorCount: number
    paletteCount: number
    imageX: number
    imageY: number
    width: number
    height: number
    bitfieldSize: number
    imageData: number[]
}

export type ColorData = {
    r: number
    g: number
    b: number
    a: number
}

const readPalettes = (view: DataView, palOfs: number) => {
    let ofs = palOfs

    const tim: TimHeader = {
        decompressedSize: view.getUint16(ofs + 0x04, true),
        paletteX: view.getUint16(ofs + 0x0c, true),
        paletteY: view.getUint16(ofs + 0x0e, true),
        colorCount: view.getUint16(ofs + 0x10, true),
        paletteCount: view.getUint16(ofs + 0x12, true),
        imageX: view.getUint16(ofs + 0x14, true),
        imageY: view.getUint16(ofs + 0x16, true),
        width: view.getUint16(ofs + 0x18, true),
        height: view.getUint16(ofs + 0x1a, true),
        bitfieldSize: view.getUint16(ofs + 0x24, true),
        imageData: [],
    }

    console.log(tim)

    const decompressedBytes = readBitField(ofs + 0x30, tim, view)
    if (decompressedBytes.byteLength !== tim.decompressedSize) {
        throw new Error('Invalid decompression length detected for image')
    }

    switch (tim.colorCount) {
        case 16:
            tim.width *= 4
            break
        case 256:
            tim.width *= 2
            break
        default:
            tim.paletteCount *= tim.colorCount / 16
            tim.colorCount = 16
            tim.width *= 4
            break
    }

    const bufferView = new DataView(decompressedBytes)
    const palettes: ColorData[][] = []
    ofs = 0
    for (let i = 0; i < tim.paletteCount; i++) {
        const palette: ColorData[] = []
        for (let k = 0; k < tim.colorCount; k++) {
            const word = bufferView.getUint16(ofs, true)
            ofs += 0x02
            const r = ((word >> 0x00) & 0x1f) << 3
            const g = ((word >> 0x05) & 0x1f) << 3
            const b = ((word >> 0x0a) & 0x1f) << 3
            const a = word > 0 ? 1 : 0
            const color: ColorData = { r, g, b, a }
            palette.push(color)
        }
        palettes.push(palette)
    }

    return palettes
}

const readBitField = (
    ofs: number,
    tim: TimHeader,
    view: DataView
): ArrayBuffer => {
    // Prepare a buffer the size of the expected output
    const buffer = new ArrayBuffer(tim.decompressedSize)
    const fp = new DataView(buffer)

    if (tim.bitfieldSize === 0) {
        for (let i = 0; i < tim.decompressedSize; i += 2) {
            const word = view.getUint16(ofs, true)
            ofs += 2
            fp.setUint16(i, word, true)
        }
        return buffer
    }

    // Read the bitfield into an array of 1's and 0's
    const bitField = []
    for (let i = 0; i < tim.bitfieldSize; i += 4) {
        const dword = view.getUint32(ofs, true)
        ofs += 4
        for (let k = 31; k > -1; k--) {
            bitField.push(dword & (1 << k) ? 1 : 0)
        }
    }

    // Then we decompress the bytes
    let outOfs = 0
    let windowOfs = 0

    for (let i = 0; i < bitField.length; i++) {
        const bit = bitField[i]
        const word = view.getUint16(ofs, true)
        if (bit === 0) {
            fp.setUint16(outOfs, word, true)
            outOfs += 2
        } else if (word === 0xffff) {
            windowOfs += 0x2000
        } else {
            let val = (word >> 3) & 0x1fff
            let copyFrom = windowOfs + val
            let copyLen = (word & 0x07) + 2
            while (copyLen--) {
                const w = fp.getUint16(copyFrom, true)
                copyFrom += 2
                fp.setUint16(outOfs, w, true)
                outOfs += 2
            }
        }
        if (outOfs >= tim.decompressedSize) {
            break
        }
        ofs += 2
    }

    return buffer
}

const readImage = (view: DataView, imgOfs: number) => {
    let ofs = imgOfs

    const tim: TimHeader = {
        decompressedSize: view.getUint16(ofs + 0x04, true),
        paletteX: view.getUint16(ofs + 0x0c, true),
        paletteY: view.getUint16(ofs + 0x0e, true),
        colorCount: view.getUint16(ofs + 0x10, true),
        paletteCount: view.getUint16(ofs + 0x12, true),
        imageX: view.getUint16(ofs + 0x14, true),
        imageY: view.getUint16(ofs + 0x16, true),
        width: view.getUint16(ofs + 0x18, true),
        height: view.getUint16(ofs + 0x1a, true),
        bitfieldSize: view.getUint16(ofs + 0x24, true),
        imageData: [],
    }

    switch (tim.colorCount) {
        case 16:
            tim.width *= 4
            break
        case 256:
            tim.width *= 2
            break
        default:
            tim.paletteCount *= tim.colorCount / 16
            tim.colorCount = 16
            tim.width *= 4
            break
    }

    const decompressedBytes = readBitField(ofs + 0x30, tim, view)
    if (decompressedBytes.byteLength !== tim.decompressedSize) {
        throw new Error('Invalid decompression length detected for image')
    }

    const bufferView = new DataView(decompressedBytes)
    const { colorCount, paletteCount } = tim
    ofs = colorCount * paletteCount * 2
    while (ofs < decompressedBytes.byteLength) {
        const byte = bufferView.getUint8(ofs++)
        if (tim.colorCount === 256) {
            tim.imageData.push(byte)
        } else {
            tim.imageData.push(byte & 0xf)
            tim.imageData.push(byte >> 4)
        }
    }

    return tim
}

const renderImage = (palettes: ColorData[][], tim: TimHeader) => {
    const canvas = document.createElement('canvas')
    canvas.width = 256
    canvas.height = 256

    const context = canvas.getContext('2d')
    const palette = palettes[0]

    const xOfs = ((tim.imageX - 320) % 64) * 4
    const yOfs = tim.imageY % 256

    context!.fillStyle = `rgba(255, 0, 0, 1))`
    context!.fillRect(0, 0, 256, 256)

    let ofs = 0
    for (let y = 0; y < tim.height; y++) {
        for (var x = 0; x < tim.width; x++) {
            const colorIndex = tim.imageData[ofs++]
            const { r, g, b, a } = palette[colorIndex]
            context!.fillStyle = `rgba(${r},${g},${b},${a})`
            context!.fillRect(x + xOfs, y + yOfs, 1, 1)
        }
    }

    return canvas
}

export { readPalettes, readImage, renderImage }
