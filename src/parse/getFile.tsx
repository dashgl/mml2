import * as localForage from 'localforage'

const getFile = async (path: string) => {
    // Load the Image file
    const localFile = await localForage.getItem(path)
    if (localFile) {
        return localFile
    }

    const req = await fetch(`DAT/${path}`)
    const remoteFile = await req.arrayBuffer()
    await localForage.setItem(path, remoteFile)
    return remoteFile
}

export { getFile }
