import { getFile } from './getFile'

import {
    Bone,
    BufferGeometry,
    Float32BufferAttribute,
    Matrix4,
    MeshBasicMaterial,
    Skeleton,
    SkinnedMesh,
    Uint16BufferAttribute,
    Vector3,
    Texture,
    DoubleSide,
    Euler,
    Quaternion,
    AnimationClip,
} from 'three'
import { Character, TimTexture } from '../types'
import { readPalettes, readImage, renderImage } from './parseTextures'

type FaceIndex = {
    materialIndex: number
    boneIndex: number
    x: number
    y: number
    z: number
    u: number
    v: number
}

type WeightedVertex = {
    pos: Vector3
    boneIndex: number
}

type DrawCall = {
    start: number
    count: number
    materialIndex: number
}

const SCALE = 0.00125
const ROT = new Matrix4()
ROT.makeRotationX(Math.PI)

const VERTEX_MASK = 0b1111111111
const VERTEX_MSB = 0b1000000000
const VERTEX_LOW = 0b0111111111

const getMeshOffset = (view: DataView, id: number) => {
    const count = view.getUint32(0x0, true)
    let ofs = 0x4
    for (let i = 0; i < count; i++) {
        if (view.getUint32(ofs + 0x00, true) !== id) {
            ofs += 0x10
            continue
        }
        const meshOfs = view.getUint32(ofs + 0x04, true)
        const tracksOfs = view.getUint32(ofs + 0x08, true)
        const controlOfs = view.getUint32(ofs + 0x0c, true)
        return { meshOfs, tracksOfs, controlOfs }
    }
    throw new Error('Could not find id in ebd file')
}

const readVertex = (
    view: DataView,
    vertexOffset: number,
    vertexCount: number,
    scale: number,
    bone: Bone,
    shareVertices: boolean,
    vertices: WeightedVertex[],
    boneIndex: number,
    boneParent: number
) => {
    const localIndices: WeightedVertex[] = []

    let ofs = vertexOffset
    const haystack = bone.parent
        ? vertices
              .filter((v) => {
                  return v.boneIndex === boneParent
              })
              .map((v) => {
                  const { x, y, z } = v.pos
                  return [x.toFixed(2), y.toFixed(2), z.toFixed(2)].join(',')
              })
        : []

    for (let i = 0; i < vertexCount; i++) {
        const dword = view.getUint32(ofs, true)
        ofs += 4

        const xBytes = (dword >> 0x00) & VERTEX_MASK
        const yBytes = (dword >> 0x0a) & VERTEX_MASK
        const zBytes = (dword >> 0x14) & VERTEX_MASK

        const xHigh = (xBytes & VERTEX_MSB) * -1
        const xLow = xBytes & VERTEX_LOW

        const yHigh = (yBytes & VERTEX_MSB) * -1
        const yLow = yBytes & VERTEX_LOW

        const zHigh = (zBytes & VERTEX_MSB) * -1
        const zLow = zBytes & VERTEX_LOW

        const vec3 = new Vector3(
            (xHigh + xLow) * scale,
            (yHigh + yLow) * scale,
            (zHigh + zLow) * scale
        )
        vec3.multiplyScalar(SCALE)
        vec3.applyMatrix4(ROT)
        vec3.applyMatrix4(bone.matrixWorld)

        const vertex: WeightedVertex = {
            pos: vec3,
            boneIndex,
        }

        // If the flag is set for weighted vertices, we check to see
        // if a vertex with the same position has already been declared
        if (shareVertices) {
            const { x, y, z } = vec3
            const needle = [x.toFixed(2), y.toFixed(2), z.toFixed(2)].join(',')
            if (haystack.indexOf(needle) !== -1) {
                vertex.boneIndex = boneParent
            }
        }

        localIndices.push(vertex)
        vertices.push(vertex)
    }

    return localIndices
}

const createBufferGeometry = (faces: FaceIndex[]) => {
    const geometry = new BufferGeometry()
    const pos: number[] = []
    const uvs: number[] = []
    const skinIndices: number[] = []
    const skinWeights: number[] = []
    const drawCalls: DrawCall[] = []

    faces.forEach((face, index) => {
        const { x, y, z, u, v, boneIndex, materialIndex } = face
        pos.push(x, y, z)
        uvs.push(u, v)
        skinIndices.push(boneIndex, 0, 0, 0)
        skinWeights.push(1, 0, 0, 0)

        if (!drawCalls.length) {
            drawCalls.push({
                start: 0,
                count: 1,
                materialIndex,
            })
        } else {
            const group = drawCalls[drawCalls.length - 1]
            if (group.materialIndex === materialIndex) {
                group.count++
            } else {
                drawCalls.push({
                    start: index,
                    count: 1,
                    materialIndex,
                })
            }
        }
    })

    drawCalls.forEach(({ start, count, materialIndex }) => {
        geometry.addGroup(start, count, materialIndex)
    })

    geometry.setAttribute('position', new Float32BufferAttribute(pos, 3))
    geometry.setAttribute('uv', new Float32BufferAttribute(uvs, 2))
    geometry.setAttribute(
        'skinIndex',
        new Uint16BufferAttribute(skinIndices, 4)
    )
    geometry.setAttribute(
        'skinWeight',
        new Float32BufferAttribute(skinWeights, 4)
    )
    geometry.computeVertexNormals()

    return geometry
}

const readFace = (
    view: DataView,
    faceOffset: number,
    faceCount: number,
    isQuad: boolean,
    localIndices: WeightedVertex[],
    faces: FaceIndex[]
) => {
    const FACE_MASK = 0b1111111
    const PIXEL_TO_FLOAT_RATIO = 0.00390625
    const PIXEL_ADJUSTMEST = 0.001953125
    let ofs = faceOffset
    for (let i = 0; i < faceCount; i++) {
        const dword = view.getUint32(ofs + 0x08, true)
        const materialIndex = (dword >> 28) & 0x3

        const au = view.getUint8(ofs + 0x00)
        const av = view.getUint8(ofs + 0x01)
        const bu = view.getUint8(ofs + 0x02)
        const bv = view.getUint8(ofs + 0x03)
        const cu = view.getUint8(ofs + 0x04)
        const cv = view.getUint8(ofs + 0x05)
        const du = view.getUint8(ofs + 0x06)
        const dv = view.getUint8(ofs + 0x07)
        ofs += 0x0c

        const indexA = (dword >> 0x00) & FACE_MASK
        const indexB = (dword >> 0x07) & FACE_MASK
        const indexC = (dword >> 0x0e) & FACE_MASK
        const indexD = (dword >> 0x15) & FACE_MASK

        const a: FaceIndex = {
            materialIndex,
            boneIndex: localIndices[indexA].boneIndex,
            x: localIndices[indexA].pos.x,
            y: localIndices[indexA].pos.y,
            z: localIndices[indexA].pos.z,
            u: au * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
            v: av * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
        }

        const b: FaceIndex = {
            materialIndex,
            boneIndex: localIndices[indexB].boneIndex,
            x: localIndices[indexB].pos.x,
            y: localIndices[indexB].pos.y,
            z: localIndices[indexB].pos.z,
            u: bu * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
            v: bv * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
        }

        const c: FaceIndex = {
            materialIndex,
            boneIndex: localIndices[indexC].boneIndex,
            x: localIndices[indexC].pos.x,
            y: localIndices[indexC].pos.y,
            z: localIndices[indexC].pos.z,
            u: cu * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
            v: cv * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
        }

        const d: FaceIndex = {
            materialIndex,
            boneIndex: localIndices[indexD].boneIndex,
            x: localIndices[indexD].pos.x,
            y: localIndices[indexD].pos.y,
            z: localIndices[indexD].pos.z,
            u: du * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
            v: dv * PIXEL_TO_FLOAT_RATIO + PIXEL_ADJUSTMEST,
        }

        faces.push(a, c, b)

        if (!isQuad) {
            continue
        }

        faces.push(b, c, d)
    }
}

const parseMesh = (
    view: DataView,
    meshOffset: number,
    mats: MeshBasicMaterial[]
) => {
    // Get number of defined
    const submeshCount = view.getUint8(meshOffset + 0x00)

    // Read the Level-of-Detail Mesh Offsets
    const geometryOffset = view.getUint32(meshOffset + 0x04, true)

    // Get the offsets to the bones
    const skeletonOffset = view.getUint32(meshOffset + 0x10, true)
    const heirarchyOffset = view.getUint32(meshOffset + 0x14, true)

    // Get offsets to the textures
    const textureOffset = view.getUint32(meshOffset + 0x18, true)

    // Read Bones
    let ofs = skeletonOffset
    const bones: Bone[] = []
    const nbBones = Math.floor((heirarchyOffset - skeletonOffset) / 6)
    for (let i = 0; i < nbBones; i++) {
        // Read Bone Position
        const x = view.getInt16(ofs + 0x00, true)
        const y = view.getInt16(ofs + 0x02, true)
        const z = view.getInt16(ofs + 0x04, true)
        ofs += 6

        // Create Threejs Bone
        const bone = new Bone()
        bone.name = `bone_${i.toString().padStart(3, '0')}`
        const vec3 = new Vector3(x, y, z)
        vec3.multiplyScalar(SCALE)
        vec3.applyMatrix4(ROT)
        bone.position.x = vec3.x
        bone.position.y = vec3.y
        bone.position.z = vec3.z
        bones.push(bone)
    }

    // Read hierarchy
    ofs = heirarchyOffset
    const hierarchy = []
    const nbSegments = (textureOffset - heirarchyOffset) / 4
    for (let i = 0; i < nbSegments; i++) {
        const polygonIndex = view.getInt8(ofs + 0x00)
        const boneParent = view.getInt8(ofs + 0x01)
        const boneIndex = view.getUint8(ofs + 0x02)
        const flags = view.getUint8(ofs + 0x03)
        const hidePolygon = Boolean(flags & 0x80)
        const shareVertices = Boolean(flags & 0x40)

        if (bones[boneIndex] && bones[boneParent] && !bones[boneIndex].parent) {
            bones[boneParent].add(bones[boneIndex])
        }

        if (flags & 0x3f) {
            console.error(`Unknown Flag: 0x${(flags & 0x3f).toString(16)}`)
        }

        hierarchy.push({
            polygonIndex,
            boneIndex,
            boneParent,
            hidePolygon,
            shareVertices,
        })
        ofs += 4
    }

    bones.forEach((bone) => {
        bone.updateMatrix()
        bone.updateMatrixWorld()
    })

    // Read Geometry
    ofs = geometryOffset
    const vertices: WeightedVertex[] = []
    const faces: FaceIndex[] = []
    for (let i = 0; i < submeshCount; i++) {
        const { boneIndex, boneParent, shareVertices } = hierarchy[i]
        const bone = bones[boneIndex]
        // Read Vertex offset and count
        const vertexOfs = view.getUint32(ofs + 0x0c, true)
        const vertexCount = view.getUint8(ofs + 0x02)
        // Read Scale
        const scaleBytes = view.getInt8(ofs + 0x03)
        const scale = scaleBytes === -1 ? 0.5 : 1 << scaleBytes
        // Read Vertices
        const localIndices: WeightedVertex[] = readVertex(
            view,
            vertexOfs,
            vertexCount,
            scale,
            bone,
            shareVertices,
            vertices,
            boneIndex,
            boneParent
        )

        // Read triangle offset and count
        const faceTriCount = view.getUint8(ofs + 0x00)
        const triFaceOfs = view.getUint32(ofs + 0x04, true)

        // Read Triangles Faces
        readFace(view, triFaceOfs, faceTriCount, false, localIndices, faces)

        // Read quads offset and count
        const faceQuadCount = view.getUint8(ofs + 0x01)
        const quadFaceOfs = view.getUint32(ofs + 0x08, true)

        // Read Quad Faces
        readFace(view, quadFaceOfs, faceQuadCount, true, localIndices, faces)
        ofs += 0x10
    }

    const geometry = createBufferGeometry(faces)
    const mesh = new SkinnedMesh(geometry, mats)
    const skeleton = new Skeleton(bones)

    const rootBone = skeleton.bones[0]
    mesh.add(rootBone)
    mesh.bind(skeleton)
    return mesh
}

const getDebugMaterials = () => {
    return [
        new MeshBasicMaterial({
            color: 0xff0000,
        }),
        new MeshBasicMaterial({
            color: 0xff00,
        }),
        new MeshBasicMaterial({
            color: 0xff,
        }),
        new MeshBasicMaterial({
            color: 0xffff00,
        }),
    ]
}

const readTextures = async (textures: TimTexture[]) => {
    const mats: MeshBasicMaterial[] = []

    for (let i = 0; i < textures.length; i++) {
        const texture = textures[i]

        // Load the Image file
        const img = await getFile(texture.imgFile)

        // Load the palette file
        const pal = await getFile(texture.palFile)

        // Create views for the files
        const imgView = new DataView(img as ArrayBuffer)
        const palView = new DataView(pal as ArrayBuffer)

        const palettes = readPalettes(palView, texture.palOffset)
        const tim = readImage(imgView, texture.imgOffset)
        const canvas = renderImage(palettes, tim)

        const tex = new Texture(canvas)
        tex.flipY = false
        tex.needsUpdate = true

        const mat = new MeshBasicMaterial({
            color: 0xffffff,
            map: tex,
            alphaTest: 0.05,
            transparent: true,
            side: DoubleSide,
        })
        mats.push(mat)
    }

    return mats
}

type AnimationHierarchy = {
    parent: number
    keys: AnimationKey[]
}

type AnimationDefintion = {
    name: string
    fps: number
    length: number
    hierarchy: AnimationHierarchy[]
}

type AnimationKey = {
    time: number
    rot: number[]
    scl: number[]
    pos: number[]
}

const parseAnimations = (
    view: DataView,
    mesh: SkinnedMesh,
    tracksOfs: number,
    controlOfs: number,
    animationNames: string[]
) => {
    const animations: AnimationClip[] = []

    if (tracksOfs === 0 || controlOfs === 0) {
        return animations
    }

    let ofs = 0
    const ROT_MAGNITUDE = [90, 180, 360, 720]

    // Get list animation frames
    const controls: number[] = []
    const firstControlOfs = view.getUint32(controlOfs, true)
    for (ofs = controlOfs; ofs < firstControlOfs; ofs += 4) {
        const ptr = view.getUint32(ofs, true)
        controls.push(ptr)
    }

    // Get List of animation tracks
    const tracks: number[] = []
    const firstTrackOfs = view.getUint32(tracksOfs, true)
    for (ofs = tracksOfs; ofs < firstTrackOfs; ofs += 4) {
        const ptr = view.getUint32(ofs, true)
        tracks.push(ptr)
    }

    controls.forEach((controlOfs) => {
        const animTrack = view.getUint8(controlOfs + 0)
        const animLen = view.getUint8(controlOfs + 1)

        if (animLen === 1) {
            return
        }

        const { bones } = mesh.skeleton
        const trackOfs = tracks[animTrack]
        const stride = (bones.length + 1) * 4

        const animation: AnimationDefintion = {
            name:
                animationNames[animations.length] ||
                `anim_${animations.length.toString().padStart(3, '0')}`,
            fps: 30,
            length: (animLen - 1) / 30,
            hierarchy: [],
        }

        bones.forEach((_bone, index) => {
            animation.hierarchy.push({
                parent: index - 1,
                keys: [],
            })
        })

        ofs = controlOfs
        for (let frame = 0; frame < animLen; frame++) {
            ofs += 4
            const trackStride = view.getUint8(ofs)
            let localOfs = trackOfs + stride * trackStride

            // Get Rotation for each bone
            for (let boneIndex = 0; boneIndex < bones.length; boneIndex++) {
                const bone = bones[boneIndex]

                // Read Root Bone Position
                if (boneIndex === 0) {
                    // Read 32 bits for position
                    // const posBytes = view.getUint32(localOfs, true)
                    localOfs += 4
                    // Read position as 10 bits per axis
                    // const xRootPos = posBytes & VERTEX_MASK
                    // const yRootPos = (posBytes >> 10) & VERTEX_MASK
                    // const zRootPos = (posBytes >> 20) & VERTEX_MASK
                    // Scale Bits are the last two bits
                    // const wRootPos = (posBytes >> 30) & 0b11
                }

                // Read Rotation angles
                const rotBytes = view.getUint32(localOfs, true)
                localOfs += 4

                const x = rotBytes & VERTEX_MASK
                const y = (rotBytes >> 10) & VERTEX_MASK
                const z = (rotBytes >> 20) & VERTEX_MASK
                const w = (rotBytes >> 30) & 0b11

                const x_pos = (x & 0x200) / 0x3ff
                const y_pos = (y & 0x200) / 0x3ff
                const z_pos = (z & 0x200) / 0x3ff

                const x_neg = ((x & 0x1ff) / 0x3ff) * -1
                const y_neg = ((y & 0x1ff) / 0x3ff) * -1
                const z_neg = ((z & 0x1ff) / 0x3ff) * -1

                const rotAngles = {
                    x: (x_pos + x_neg) * ROT_MAGNITUDE[w],
                    y: (y_pos + y_neg) * ROT_MAGNITUDE[w],
                    z: (z_pos + z_neg) * ROT_MAGNITUDE[w],
                }

                const e = new Euler(
                    -(rotAngles.x * Math.PI) / 180,
                    -(rotAngles.y * Math.PI) / 180,
                    (rotAngles.z * Math.PI) / 180
                )

                const q = new Quaternion()
                q.setFromEuler(e)

                const key: AnimationKey = {
                    time: frame / 30,
                    rot: q.toArray(),
                    scl: [1, 1, 1],
                    pos: [bone.position.x, bone.position.y, bone.position.z],
                }

                animation.hierarchy[boneIndex].keys.push(key)
            }

            // End rotation
        }

        const clip = AnimationClip.parseAnimation(animation, bones)
        if (!clip) {
            return console.error('Invalid clip detected')
        }

        clip.optimize()
        animations.push(clip)
    })
    return animations
}

const parseCharacter = async (char: Character) => {
    const MAGIC_ENTITY = 0x0a
    const DEBUG_MATS = false

    // Load the BIN file that contains the model
    const bin = await getFile(char.modelFile)
    const file = bin as ArrayBuffer
    const view = new DataView(file)
    const type = view.getUint32(0, true)
    const len = view.getUint32(0x04, true)

    // Double check to make sure we're reading from the correct file type
    if (type !== MAGIC_ENTITY) {
        throw new Error(`Unexpected EBD File header: 0x${type.toString(16)}`)
    }

    // Slice out the EBD for relative pointers
    const ebdFile = file.slice(0x30, len + 0x30)
    const ebdView = new DataView(ebdFile)

    // Find the mesh offset for the requested asset
    const { meshOfs, tracksOfs, controlOfs } = getMeshOffset(ebdView, char.id)

    // Get the Materials
    const mats = DEBUG_MATS
        ? getDebugMaterials()
        : await readTextures(char.textures)
    const mesh = parseMesh(ebdView, meshOfs, mats)
    mesh.name = char.name

    const animations = parseAnimations(
        ebdView,
        mesh,
        tracksOfs,
        controlOfs,
        char.animations
    )
    return { mesh, animations }
}

export default parseCharacter
