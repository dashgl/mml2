import {
    BufferGeometry,
    Mesh,
    Material,
    SkinnedMesh,
    MeshBasicMaterial,
} from 'three'
import * as jsonld from 'jsonld'
import { v4 as uuidv4 } from 'uuid'

enum TextureFormat {
    JPG = 'JPG',
    PNG = 'PNG',
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const dashMaterials = (matsIn: Material[], textureLookup: string[]) => {
    const mats = matsIn as MeshBasicMaterial[]
    const materials: any[] = []

    mats.forEach((mat) => {
        materials.push({
            type: 'DashMaterial',
            textureIndex: mat.map ? textureLookup.indexOf(mat.map.uuid) : -1,
        })
    })

    return materials
}

const dashTextures = (matsIn: Material[]) => {
    const textures: any[] = []
    const textureLookup: string[] = []
    const mats = matsIn as MeshBasicMaterial[]

    mats.forEach((mat) => {
        if (!mat.map || !mat.map.image) {
            return
        }

        const { image } = mat.map

        const canvas = image
        const { width, height } = canvas
        const data = canvas.toDataURL()

        // const name =
        //     mat.map.name && mat.map.name.length
        //         ? mat.map.name
        //         : `texture_${textures.length.toString().padStart(3, '0')}`

        textures.push({
            type: 'DashTexture',
            width,
            height,
            format: TextureFormat.PNG,
            data: data,
        })

        textureLookup.push(mat.map.uuid)
    })

    return { textures, textureLookup }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const dashVertices = (geometry: BufferGeometry) => {
    const indexLookup: number[] = []
    const { position } = geometry.attributes
    const vertexKeys: string[] = []
    const vertices = []

    for (let i = 0; i < position.count; i++) {
        const vertex = {
            type: 'DashVertex',
            position: {
                type: 'DashVec3',
                x: position.array[i * 3 + 0],
                y: position.array[i * 3 + 1],
                z: position.array[i * 3 + 2],
            },
        }

        const key = [
            vertex.position.x.toFixed(3),
            vertex.position.y.toFixed(3),
            vertex.position.z.toFixed(3),
        ].join('*')

        if (!vertexKeys.includes(key)) {
            vertexKeys.push(key)
            vertices.push(vertex)
        }

        indexLookup[i] = vertexKeys.indexOf(key)
    }

    return {
        indexLookup,
        vertices,
    }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const dashFaces = (geometry: BufferGeometry, indexLookup: number[]) => {
    const faces = []
    const { groups } = geometry
    const position = geometry.attributes.position
    const uv = geometry.attributes.uv.array
    let uvIndex = 0

    for (let i = 0; i < position.count; i += 3) {
        // Get Material Index

        let materialIndex: number = -1
        for (let k = 0; k < groups.length; k++) {
            if (i < groups[k].start) {
                continue
            }

            if (i >= groups[k].start + groups[k].count) {
                continue
            }

            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            materialIndex = groups[k].materialIndex as number
            break
        }

        // Get Indices

        const a = indexLookup[i + 0]
        const b = indexLookup[i + 1]
        const c = indexLookup[i + 2]

        // Get Vertex uv0

        let face = {
            type: 'DashFace',
            materialIndex: 0,
            indices: {
                type: 'DashIndices',
                a,
                b,
                c,
            },
            diffuseMap: {
                type: 'DashTextureMap',
                a: {
                    type: 'DashCoord2d',
                    u: uv[uvIndex++],
                    v: uv[uvIndex++],
                },
                b: {
                    type: 'DashCoord2d',
                    u: uv[uvIndex++],
                    v: uv[uvIndex++],
                },
                c: {
                    type: 'DashCoord2d',
                    u: uv[uvIndex++],
                    v: uv[uvIndex++],
                },
            },
        }

        faces.push(face)
    }

    faces.push()
    return faces
}

const exportDashModel1 = async (mesh: Mesh | SkinnedMesh): Promise<Blob> => {
    const { indexLookup, vertices } = dashVertices(mesh.geometry)
    const faces = dashFaces(mesh.geometry, indexLookup)
    let mats = mesh.material || []
    if (!Array.isArray(mats)) {
        mats = [mats]
    }

    const { textures, textureLookup } = dashTextures(mats)
    const materials = dashMaterials(mats, textureLookup)

    const credential = {
        '@context': [
            'https://www.w3.org/2018/credentials/v1',
            {
                Verifiable3dModel: {
                    '@id': 'https://gitlab.com/dashgl/format',
                },
                DashModelFormat: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/home',
                    '@context': {
                        name: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#name',
                        },
                        copyright: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#copyright',
                        },
                        exportedFrom: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#exported-from',
                        },
                        url: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#url',
                        },
                        formatVersion: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#formatVersion',
                        },
                        faces: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#faces',
                        },
                        vertices: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#vertices',
                        },
                        materials: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#materials',
                        },
                        textures: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#textures',
                        },
                        skeleton: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/Format-Overview#skeleton',
                        },
                    },
                },
                DashFace: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0',
                    '@context': {
                        materialIndex: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#materialindex',
                        },
                        indices: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#indices',
                        },
                        diffuseMap: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#diffusemap',
                        },
                    },
                },
                DashIndices: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#dashindices',
                    '@context': {
                        a: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#dashindices-a',
                        },
                        b: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#dashindices-b',
                        },
                        c: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#dashindices-c',
                        },
                    },
                },
                DashTextureMap: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#diffusemap',
                    '@context': {
                        a: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTypes-2.0#dashindices-a',
                        },
                        b: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTypes-2.0#dashindices-b',
                        },
                        c: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTypes-2.0#dashindices-c',
                        },
                    },
                },
                DashCoord2d: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTypes-2.0#texture-coords',
                    '@context': {
                        u: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#texture-coords-u',
                        },
                        v: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashFace-2.0#texture-coords-v',
                        },
                    },
                },
                DashTexture: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTexture-2.0',
                    '@context': {
                        width: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTexture-2.0#width',
                        },
                        height: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTexture-2.0#height',
                        },
                        data: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTexture-2.0#data',
                        },
                        format: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTexture-2.0#format',
                        },
                    },
                },
                DashMaterial: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashMaterial-2.0',
                    '@context': {
                        textureIndex: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashMaterial-2.0#textureindex',
                        },
                    },
                },
                DashVertex: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashVertex-2.0',
                    '@context': {
                        position: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashVertex-2.0#position',
                        },
                    },
                },
                DashVec3: {
                    '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTypes-2.0#vec3',
                    '@context': {
                        x: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTypes-2.0#vec3-x',
                        },
                        y: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTypes-2.0#vec3-x',
                        },
                        z: {
                            '@id': 'https://gitlab.com/dashgl/format/-/wikis/DashTypes-2.0#vec3-x',
                        },
                    },
                },
            },
        ],
        '@id': `urn:uuid:${uuidv4()}`,
        type: ['VerifiableCredential', 'Verifiable3dModel'],
        issuanceDate: '2010-01-01T19:23:24Z',
        credentialSubject: {
            '@id': `model:${mesh.name}`,
            type: 'DashModelFormat',
            name: mesh.name,
            exportedFrom: 'Threejs Dash Model 2.0 Exporter',
            url: 'https://dashgl.gitlab.io/mml2/',
            formatVersion: '2.0',
            faces,
            vertices,
            textures,
            materials,
        },
    }

    const nquads = await jsonld.toRDF(credential, {
        format: 'application/n-quads',
    })
    console.log(nquads)
    const text = JSON.stringify(credential, null, 2)
    return new Blob([text])
}

export default exportDashModel1
