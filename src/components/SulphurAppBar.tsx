import AppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import { saveAs } from 'file-saver'
import { AnimationClip, Mesh, SkinnedMesh } from 'three'
import { dashExport1, dashExport2, dashExport3, dashExport4 } from '../export'
import React, { useRef } from 'react'
import { dashImport } from '../import'
export default function SulphurAppBar({
    mesh,
    setMesh,
    setAnimations,
}: {
    mesh: Mesh | SkinnedMesh | null
    setMesh: (m: Mesh | SkinnedMesh) => void
    setAnimations: (a: AnimationClip[]) => void
}) {
    const inputFile = useRef(null)
    const handleExportClick = async (v: number) => {
        if (!mesh) {
            return
        }

        if (v === 1) {
            const blob = await dashExport1(mesh)
            saveAs(blob, 'model1.txt')
        } else if (v === 2) {
            const blob = await dashExport2(mesh)
            saveAs(blob, 'model2.txt')
        } else if (v === 3) {
            const blob = await dashExport3(mesh)
            saveAs(blob, 'model3.txt')
        } else if (v === 4) {
            const blob = await dashExport4(mesh)
            saveAs(blob, 'model4.txt')
        }
    }

    const handleImportClick = async () => {
        inputFile.current && (inputFile.current as HTMLInputElement).click()
    }

    const handleFileChange = async (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        if (!(event.target as HTMLInputElement)) {
            return
        }

        const { files } = event.target as HTMLInputElement
        if (!files || !files.length) {
            return
        }

        const file = files[0]
        const text = await file.text()
        const json = JSON.parse(text)
        const { credentialSubject } = json
        if (!credentialSubject) {
            return
        }

        const mesh = dashImport(credentialSubject)
        console.log('Setting mesh!!!!')
        setMesh(mesh)
        setAnimations([])
    }

    return (
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    Megaman Legends 2
                </Typography>

                <Button
                    disabled={!mesh}
                    onClick={() => handleExportClick(1)}
                    color="inherit"
                >
                    Export1
                </Button>
                <Button
                    disabled={!mesh}
                    onClick={() => handleExportClick(2)}
                    color="inherit"
                >
                    Export2
                </Button>
                <Button
                    disabled={!mesh}
                    onClick={() => handleExportClick(3)}
                    color="inherit"
                >
                    Export3
                </Button>
                <Button
                    disabled={!mesh}
                    onClick={() => handleExportClick(4)}
                    color="inherit"
                >
                    Export4
                </Button>
                <Button onClick={handleImportClick} color="inherit">
                    Import
                </Button>
                <input
                    type="file"
                    hidden={true}
                    ref={inputFile}
                    onChange={handleFileChange}
                />

                <Button href="https://gitlab.com/dashgl/mml2/" color="inherit">
                    Gitlab
                </Button>
            </Toolbar>
        </AppBar>
    )
}
