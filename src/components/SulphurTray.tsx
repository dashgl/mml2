import { useState } from 'react'
import { Box } from '@mui/material/'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import { characters } from '../data'
import { parseCharacter } from '../parse'
import { SomeMesh, Character } from '../types'
import { AnimationClip } from 'three'

export default function SulphurTray({
    width,
    setMesh,
    setAnimations,
    setAnim,
}: {
    width: number
    setMesh: (m: SomeMesh) => void
    setAnimations: (a: AnimationClip[]) => void
    setAnim: (a: string) => void
}) {
    const [active, setActive] = useState('')

    const loadCharacter = async (char: Character) => {
        const { mesh, animations } = await parseCharacter(char)
        setActive(char.name)
        setMesh(mesh)
        setAnimations(animations)
        setAnim('')
    }

    return (
        <Box
            sx={{
                width,
                height: '100%',
                maxHeight: '100%',
                overflow: 'hidden',
                bgcolor: 'background.paper',
                display: 'flex',
            }}
        >
            <List
                sx={{
                    width: '100%',
                    m: 0,
                    p: 0,
                    flex: '0 0 auto',
                    overflowY: 'scroll',
                }}
            >
                {characters.map((char) => {
                    return (
                        <ListItem
                            selected={char.name === active}
                            key={char.name}
                            divider={true}
                            onClick={() => {
                                loadCharacter(char)
                            }}
                        >
                            <ListItemText
                                primary={char.name}
                                secondary={char.modelFile}
                            />
                        </ListItem>
                    )
                })}
            </List>
        </Box>
    )
}
