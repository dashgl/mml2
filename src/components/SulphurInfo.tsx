import {
    Box,
    SelectChangeEvent,
    TableContainer,
    Table,
    TableCell,
    TableRow,
    TableBody,
    Radio,
} from '@mui/material'
import { Mesh, SkinnedMesh, AnimationClip } from 'three'

export default function SulphurInfo({
    width,
    mesh,
    animations,
    anim,
    setAnim,
}: {
    width: number
    mesh: Mesh | SkinnedMesh | null
    animations: AnimationClip[]
    anim: string
    setAnim: (a: string) => void
}) {
    const handleChange = (event: SelectChangeEvent) => {
        console.log('CHange!!!')
        console.log(event.target.value)
        setAnim(event.target.value as string)
    }

    const anims = animations || []

    return (
        <Box
            sx={{
                width,
                height: '100%',
                maxHeight: '100%',
                overflow: 'hidden',
                bgcolor: 'background.paper',
                display: 'flex',
            }}
        >
            <TableContainer sx={{ flex: '0 0 auto', overflowY: 'scroll' }}>
                <Table aria-label="simple table">
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Radio
                                    checked={anim === ''}
                                    onChange={handleChange}
                                    value=""
                                    name="play-animation"
                                />
                            </TableCell>
                            <TableCell>Stop Animation</TableCell>
                        </TableRow>
                        {anims.map(({ name }) => {
                            return (
                                <TableRow key={name}>
                                    <TableCell>
                                        <Radio
                                            checked={anim === name}
                                            onChange={handleChange}
                                            value={name}
                                            name="play-animation"
                                        />
                                    </TableCell>
                                    <TableCell>{name}</TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Box>
    )
}
