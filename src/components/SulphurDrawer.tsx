import { useState } from 'react'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import Divider from '@mui/material/Divider'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'

export default function SulphurDrawer({ width }: { width: number }) {
    const drawer = ['Characters', 'Tiles', 'Maps', 'Player']
    const [active, setActive] = useState(drawer[0])
    return (
        <Box sx={{ width, backgroundColor: '#fcfcfc', display: 'flex' }}>
            <List sx={{ width: '100%' }}>
                {drawer.map((text) => (
                    <ListItem
                        key={text}
                        selected={text === active}
                        disablePadding
                        onClick={() => {
                            setActive(text)
                        }}
                    >
                        <ListItemButton>
                            <ListItemText primary={text} />
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
            <Divider />
        </Box>
    )
}
