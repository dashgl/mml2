import React, { Component } from 'react'
import {
    Clock,
    Scene,
    PerspectiveCamera,
    WebGLRenderer,
    PointLight,
    GridHelper,
    AnimationClip,
    AnimationMixer,
    Object3D,
    AnimationAction,
    BoxHelper,
} from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { SomeMesh } from '../types'

type CanvasParams = {
    mesh: SomeMesh
    animations: AnimationClip[]
    anim: string
}

class Canvas extends Component<CanvasParams> {
    mount: HTMLCanvasElement | null
    renderer: WebGLRenderer | null
    camera: PerspectiveCamera
    scene: Scene
    mesh: SomeMesh
    action: AnimationAction | null
    animations: AnimationClip[]
    grid: GridHelper
    mixer: AnimationMixer | null
    clock: Clock
    anim: string
    box: BoxHelper | null

    constructor(props: CanvasParams) {
        super(props)
        this.mount = null
        this.renderer = null
        this.clock = new Clock()
        this.camera = new PerspectiveCamera(75, 1, 0.1, 1000)
        this.scene = new Scene()
        this.mesh = null
        this.animations = []
        this.action = null
        this.mixer = null
        this.grid = new GridHelper(50, 20, 0xff0000, 0xffffff)
        this.anim = ''
        this.box = null
    }

    componentDidMount() {
        this.createScene()
        if (this.mesh) {
            this.scene.add(this.mesh!)
        }
        this.updateDimensions()
        this.animationLoop()
        window.addEventListener('resize', this.updateDimensions)
    }

    createScene() {
        this.renderer = new WebGLRenderer({
            canvas: this.mount!,
            antialias: true,
            alpha: true,
            preserveDrawingBuffer: true,
        })
        this.renderer.setClearColor(0x000000, 0)
        new OrbitControls(this.camera, this.mount as HTMLElement)
        const lights: PointLight[] = []
        lights[0] = new PointLight(0xffffff, 1, 0)
        lights[1] = new PointLight(0xffffff, 1, 0)
        lights[2] = new PointLight(0xffffff, 1, 0)

        lights[0].position.set(0, 200, 0)
        lights[1].position.set(100, 200, 100)
        lights[2].position.set(-100, -200, -100)

        this.camera.position.set(20, 40, 0)

        this.scene.add(lights[0])
        this.scene.add(lights[1])
        this.scene.add(lights[2])
        this.scene.add(this.grid)
    }

    updateDimensions() {
        if (!this.mount) {
            return
        }
        const { clientWidth, clientHeight } = this.mount.parentElement!
        this.renderer!.setSize(clientWidth, clientHeight - 5)
        this.camera.aspect = clientWidth / clientHeight
        this.camera.updateProjectionMatrix()
    }

    animationLoop() {
        requestAnimationFrame(this.animationLoop.bind(this))

        if (this.mesh) {
            //this.mesh!.rotation.x += 0.01;
            //this.mesh!.rotation.y += 0.01;
        }

        if (this.mixer) {
            const delta = this.clock.getDelta()
            this.mixer.update(delta)
        }
        this.renderer!.render(this.scene, this.camera)
    }

    render() {
        const { mesh, animations, anim } = this.props
        console.log('Debug Anim', anim)

        if (mesh !== this.mesh) {
            if (this.mesh) {
                this.scene.remove(this.mesh)
            }
            this.scene.add(mesh as Object3D)
            this.mesh = mesh
            if (this.box) {
                this.scene.remove(this.box)
            }

            this.box = new BoxHelper(mesh as Object3D, 0xff0000)
            this.scene.add(this.box)
        }

        if (anim === '' && this.action) {
            this.action.stop()
        }

        if (anim !== '' && anim !== this.anim) {
            if (this.action) {
                this.action.stop()
            }
            this.anim = anim
            const mixer = new AnimationMixer(mesh as Object3D)
            const clip = animations.find(({ name }) => name === anim)
            if (clip) {
                const action = mixer.clipAction(clip, mesh as Object3D)
                this.action = action
                action.play()
                this.mixer = mixer
            }
        }

        return <canvas ref={(ref) => (this.mount = ref)} />
    }
}

const SulphurCanvas = ({ mesh, animations, anim }: CanvasParams) => {
    const viewport: React.CSSProperties = {
        flexGrow: 2,
        backgroundColor: '#8b95a6',
        height: '100%',
        overflow: 'hidden',
    }
    return (
        <div style={viewport}>
            <Canvas mesh={mesh} animations={animations} anim={anim} />
        </div>
    )
}

export default SulphurCanvas
