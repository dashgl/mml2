import { Mesh, SkinnedMesh } from 'three'

export type TimTexture = {
    imgFile: string
    imgOffset: number
    palFile: string
    palOffset: number
}

export type Character = {
    name: string
    id: number
    tag: string
    modelFile: string
    otherFiles: string[]
    textures: TimTexture[]
    animations: string[]
}

export type SomeMesh = null | SkinnedMesh | Mesh
