import React, { useState } from 'react'
import {
    SulphurAppBar,
    SulphurCanvas,
    SulphurDrawer,
    SulphurInfo,
    SulphurTray,
} from './components'
import { Box, Stack, Divider } from '@mui/material'
import { AnimationClip, Mesh, SkinnedMesh } from 'three'

function App() {
    const [anim, setAnim] = useState('')
    const [mesh, setMesh] = useState<Mesh | SkinnedMesh | null>(null)
    const [animations, setAnimations] = useState<AnimationClip[]>([])

    return (
        <Box
            sx={{
                height: '100%',
                display: 'flex',
                flexFlow: 'column',
                overflow: 'hidden',
            }}
        >
            <SulphurAppBar
                mesh={mesh}
                setMesh={setMesh}
                setAnimations={setAnimations}
            />
            <Stack
                direction="row"
                divider={<Divider orientation="vertical" flexItem />}
                sx={{ height: '100%', overflow: 'hidden' }}
            >
                <SulphurDrawer width={140} />
                <SulphurTray
                    setMesh={setMesh}
                    setAnimations={setAnimations}
                    setAnim={setAnim}
                    width={260}
                />
                <SulphurCanvas
                    mesh={mesh}
                    animations={animations}
                    anim={anim}
                />
                <SulphurInfo
                    width={340}
                    mesh={mesh}
                    animations={animations}
                    anim={anim}
                    setAnim={setAnim}
                />
            </Stack>
        </Box>
    )
}

export default App
